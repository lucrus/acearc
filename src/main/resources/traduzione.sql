UPDATE Contatto as C
SET C.dieta = "1"
WHERE C.nomaiale = true;

UPDATE Contatto as C
SET C.dieta = "2"
WHERE C.vegetariano = true;

UPDATE Contatto as C
SET C.dieta = "3"
WHERE C.vegano = true;

UPDATE Contatto as C
SET C.dieta = "0"
WHERE C.dieta IS NULL;

UPDATE Contatto as C
SET C.allergie = C.altreIndicazioni;

UPDATE Contatto as C
SET C.telefono = C.telefonoFisso;

UPDATE Iscrizione as I
SET I.consensoTrattamentoDati = I.accettaPrivacyPerQuestaIscrizione;

UPDATE Iscrizione as I
SET I.consensoIndirizzario = I.accettaPrivacyIndirizzario;

UPDATE Iscrizione as I
SET I.consensoImmagineWeb = I.autorizzazioneFoto;

UPDATE Iscrizione as I
SET I.consensoNewsletter = I.newsletterPerQuestaIscrizione;

UPDATE Campo as C
SET C.trafiletto = C.descrizione;

UPDATE Campo as C
SET C.quotaRidotta = C.quotaIscrizione1;

UPDATE Campo as C
SET C.quota0 = C.quotaIscrizione2;

UPDATE Campo as C
SET C.quota1 = C.quotaIscrizione3;

UPDATE Campo as C
SET C.quota2 = C.quotaIscrizione4;

UPDATE Campo as C
SET C.postiTotali = C.numeroPostiDisponibili;

UPDATE Ospite as O
SET O.consensoNewsletter = O.newsletter;
