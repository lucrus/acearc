package it.virtualbit.acearc.model;

import java.io.*;
import java.util.*;

import javax.persistence.*;

import org.openxava.annotations.*;

import lombok.*;

@Entity @Getter @Setter
public class Recapito extends AceArcId implements Serializable {
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch=FetchType.LAZY, optional=false)
	@DescriptionsList(descriptionProperties = "cognome, nome, dataNascita, codiceFiscale") 
	private Contatto contatto; 

	@Column(length=254)
    private String email;
	
	@Column(length=254)
	private String telefono;
	
	@Column(length=254)
	private String sito;

	@Column(length=254)
	private String facebook;

	@Column(length=254)
	private String instagram;
	
	@Column(length=254)
	private String tiktok;
	
	@Column(length=254)
	private String twitter;
	
	@Column(length=254)
	private String discord;
	
	@Column(length=254)
	private String youtube;
	
	@Column(length=254)
	private String linkedin;

	@Column(length=254)
	private String altroSocial;

	@Column()
	private Date data;
}
