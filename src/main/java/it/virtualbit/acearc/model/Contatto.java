package it.virtualbit.acearc.model;

import java.io.*;
import java.util.*;

import javax.persistence.*;

import org.openxava.annotations.*;

import lombok.*;

@Entity @Getter @Setter
@Tab(properties="appellativo, pronomi, nome, cognome, dataNascita, telefono, email")
@View(name="Simple", members="appellativo; pronomi; nome; cognome; dataNascita; lingueParlate; email; telefono;")
@View(members=
	"appellativo;" +
	"pronomi;" +
	"email;" +
	"telefono;" +
	"lingueParlate;" +
	"ultimoConsensoTrattamentoDati;" +
	"notifiche [" +
		"nome;" +
		"cognome;" +
		"sesso;" +
		"dataNascita;" +
		"luogoNascita;" +
		"comune;" +
		"nazione;" +
		"cittadinanza;" +
	"]" +
	"dieta [" +
		"dieta;" +
		"allergie;" +
		"intolleranze;" +
	"]" +
	"altro [" +
		"indirizzo;" +
		"cap;" +
		"provincia;" +
		"codiceFiscale;" +
		"professione;" +
	"];" +
	"collaborazioni;" +
	"campiEdEventi;" +
	"ospite;"
)
public class Contatto extends AceArcId implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(length=80)
    private String cognome;

	@Column(length=80)
    private String nome;

	@Column(length=80)
    private String appellativo;

	@Column()
	private Pronomi pronomi;
	public enum Pronomi { Femminili, Maschili, Neutri, Entrambi, Tutti };

	@Column()
	@Enumerated(EnumType.STRING)
	private Sesso sesso;

	@Column()
    private Date dataNascita;

	@Column(length=80)
    private String luogoNascita;

	@Column(length=18)
	private String codiceFiscale;

	@Column(length=120)
    private String indirizzo;

	@Column(length=120)
    private String comune;

	@Column(length=120)
    private String cap;

	@Column(length=120)
    private String provincia;

	@Column(length=254)
	private String telefono;

	@Column(length=254)
    private String email;

	@Column(length=120)
    private String nazione;

	@Column(length=80)
    private String cittadinanza;

	@Column(length=80)
    private String lingueParlate;

	@Column(length=80)
    private String professione;

	@Column()
    private Date ultimoConsensoTrattamentoDati;

	@ManyToOne(fetch=FetchType.LAZY,optional=true)
	//@DescriptionsList(descriptionProperties = "codice")
	private Lingua linguaPreferita;

	@ManyToOne(fetch=FetchType.LAZY,optional=true)
	//@DescriptionsList(descriptionProperties = "codice")
	private Lingua linguaAlternativa;

	@OneToMany(mappedBy="contatto",cascade=CascadeType.REMOVE)
	//@ListProperties("tipo.abbreviazione,numero")
	private Collection<DocumentoId> documenti;

	@OneToMany(mappedBy="contatto",cascade=CascadeType.REMOVE)
	//@ListProperties("email,telefono")
	private Collection<Recapito> altriRecapiti;

	@OneToMany(mappedBy="contatto",cascade=CascadeType.REMOVE)
	@ListProperties("stato, campo.codice, campo.inizio, campo.titolo, ruolo, arrivo, oraDiArrivo, partenza, oraDiPartenza, consensoTrattamentoDati")
	private Collection<Iscrizione> campiEdEventi;

	@OneToMany(mappedBy="contatto",cascade=CascadeType.REMOVE)
	//@ListProperties("ruolo.nome, incarico, inizio, fine") // Thus the reference is displayed using a combo
	private Collection<Collaborazione> collaborazioni;

	@OneToMany(mappedBy="contatto",cascade=CascadeType.REMOVE)
	@ListProperties("stato, pensione, arrivo, oraDiArrivo, partenza, oraDiPartenza, adulti, bambini, neonati, preventivo, quotaVersata, consensoTrattamentoDati") // Thus the reference is displayed using a combo
	private Collection<Ospite> ospite;

	@Column()
	private Dieta dieta;
	public enum Dieta { flexitariana, noMaiale, vegetariana, vegana };

	@Column(length=1000)
    private String allergie;

	@Column(length=300)
    private String intolleranze;

	@Column()
    private Boolean vegetariano;

	@Column()
    private Boolean vegano;

	@Column()
    private Boolean noLatticini;

	@Column()
    private Boolean celiaco;

	@Column()
    private Boolean noMaiale;

	@Column(length=1000)
	@Stereotype("MEMO")
    private String altreIndicazioni;
}

/*
 *
 LOAD DATA INFILE '/tmp/persone.csv' REPLACE INTO TABLE Contatto FIELDS TERMINATED BY '|' LINES TERMINATED BY '\n' IGNORE 1 LINES (uuid,nome,cognome,sesso,luogoNascita,cittadinanza,indirizzo,cap,comune,provincia,nazione,telefonoFisso,email,@vegetariano,altreIndicazioni) set vegetariano=cast(@vegetariano as signed);
*/