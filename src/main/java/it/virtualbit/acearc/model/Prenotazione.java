package it.virtualbit.acearc.model;

import java.io.*;
import java.text.*;
import java.util.*;

import javax.persistence.*;

import org.openxava.annotations.*;

import lombok.*;

@Entity @Getter @Setter
public class Prenotazione extends AceArcId implements Serializable {

	private static final long serialVersionUID = 1L;
        private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    @ManyToOne(fetch=FetchType.LAZY, optional=false)
	private Stanza stanza;

    @ManyToOne(fetch=FetchType.LAZY, optional=true)
	private Ospite ospite;
}
