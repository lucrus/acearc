package it.virtualbit.acearc.model;

import java.io.*;
import java.text.*;
import java.util.*;

import javax.persistence.*;

import org.openxava.annotations.*;

import lombok.*;

@Entity @Getter @Setter
@Tab(properties="codice, inizio, nome, titolo, quotaRidotta, quota0, quota1, quota2, linkEventoOnline")
@View(members=
	"codice, nome, titolo;" +
	"linkEventoOnline;" +
	"date [ inizio, oraDiInizio, fine, oraDiFine ];" +
	"posti [" +
		"postiTotali, postiNuovi;" +
		"postiComunita, postiPartnership;" +
	"];" +
	"eta [" +
		"etaMinima, etaMassima;" +
		"classeMin, classeMax;" +
	"];" +
	"quote [" +
		"quotaRidotta;" +
		"quota0;" +
		"quota1;" +
		"quota2;" +
		"caparra;" +
	"], borseCampo [" +
		"agape1, campista1;" +
		"agape2, campista2;" +
		"agape3, campista3;" +
	"];" +
	"trafiletto;" +
	"iscritti;"
)
public class Campo extends AceArcId implements Serializable {

	private static final long serialVersionUID = 1L;
        private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	@Column(length=5)
    private String codice;

	@Column(length=30)
    private String nome;

	@Column(length=50)
    private String titolo;

	@Column(length=3000)
	@Stereotype("MEMO")
	private String trafiletto;

	@Column()
    private Date inizio;

	@Stereotype("TIME")
	@Column()
	private String oraDiInizio;

	@Column()
    private Date fine;

	@Stereotype("TIME")
	@Column()
	private String oraDiFine;

	@Stereotype("MONEY")
	@Column()
    private Float quotaRidotta;

	@Stereotype("MONEY")
	@Column()
    private Float quota0;

	@Stereotype("MONEY")
	@Column()
    private Float quota1;

	@Stereotype("MONEY")
	@Column()
    private Float quota2;

	@Stereotype("MONEY")
	@Column()
    private Float caparra;

	@Stereotype("MONEY")
	@Column()
    private Float agape1;

	@Stereotype("MONEY")
	@Column()
    private Float campista1;

	@Stereotype("MONEY")
	@Column()
    private Float agape2;

	@Stereotype("MONEY")
	@Column()
    private Float campista2;

	@Stereotype("MONEY")
	@Column()
    private Float agape3;

	@Stereotype("MONEY")
	@Column()
    private Float campista3;

	@Column()
	private Integer postiTotali;

	@Column()
	private Integer postiNuovi;

	@Column()
	private Integer postiComunita;

	@Column()
	private Integer postiPartnership;

	@Column()
    private Integer etaMinima;

	@Column()
    private Integer etaMassima;

	@Column()
	private Classe classeMin;

	@Column()
	private Classe classeMax;
	public enum Classe { Elem1, Elem2, Elem3, Elem4, Elem5, Med1, Med2, Med3, Sup1, Sup2, Sup3, Sup4, Sup5};

	@Column(length=300)
	private String linkEventoOnline;

	@OneToMany(mappedBy="campo")
	@ListProperties("stato, ruolo, contatto.appellativo, contatto.pronomi, contatto.nome, contatto.cognome, arrivo, oraDiArrivo, partenza, oraDiPartenza, quotaScelta, quotaVersata, consensoTrattamentoDati")
	private Collection<Iscrizione> iscritti;

	public String getDescriptionForLists() {
	  return codice + " " + sdf.format(inizio);
	}
}
