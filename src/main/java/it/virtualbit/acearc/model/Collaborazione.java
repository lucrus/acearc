package it.virtualbit.acearc.model;

import java.io.*;
import java.util.*;

import javax.persistence.*;

import org.openxava.annotations.*;

import lombok.*;

@Entity @Getter @Setter
@View(members=
	"contatto;" +
	"ruolo [ruolo; incarico]" +
	"date [inizio; fine];" +
	"consensoTrattamentoDati;" +
	"consensoImmagineInt;" +
	"consensoImmagineWeb;" +
	"consensoNewsletter;" +
	"consensoIndirizzario;"
)
public class Collaborazione extends AceArcId implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch=FetchType.LAZY, optional=false)
	@DescriptionsList(descriptionProperties = "cognome, nome, appellativo, dataNascita, codiceFiscale")
	private Contatto contatto;

	@Column()
	@Enumerated(EnumType.STRING)
	private RuoloCollaboratore ruolo;

	@Column(length=100)
	private String incarico;

	@Column()
    private Date inizio;

	@Column()
    private Date fine;

	@Column()
    private Boolean consensoTrattamentoDati;

	@Column()
    private Boolean consensoImmagineInt;

	@Column()
    private Boolean consensoImmagineWeb;

	@Column()
	private Boolean consensoNewsletter;

	@Column()
    private Boolean consensoIndirizzario;
}
