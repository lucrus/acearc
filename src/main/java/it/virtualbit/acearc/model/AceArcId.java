package it.virtualbit.acearc.model;

import java.io.*;
import java.util.*;

import javax.persistence.*;

import org.openxava.annotations.*;

import lombok.*;

@MappedSuperclass @Getter @Setter
public abstract class AceArcId  implements Serializable {
  
  private static final long serialVersionUID = 1L;
  
  @Id @Hidden
  @Basic(optional = false, fetch = FetchType.EAGER)
  @Column(name = "uuid")
  private String uuid;
  
  @Column() @Hidden
  private Boolean deleted;
  
  @Column() @Hidden
  private Date createdAt;

  public AceArcId()
  {
  }

  public AceArcId(String uuid)
  {
    this.uuid = uuid;
  }
      
  @Override
  public int hashCode()
  {
    int hash = 0;
    hash += (uuid != null ? uuid.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object)
  {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof AceArcId))
    {
      return false;
    }
    AceArcId other = (AceArcId) object;
    if ((this.uuid == null && other.getUuid() != null) || (this.uuid != null && !this.uuid.equals(other.getUuid())))
      return false;
    return true;
  }

  @Override
  public String toString()
  {
    return getClass().getName() + "[ uuid=" + uuid + " ]";
  }
  
  @PrePersist void onPrePersist() 
  {
    if (uuid == null)
      uuid = UUID.randomUUID().toString();
  }  
}
