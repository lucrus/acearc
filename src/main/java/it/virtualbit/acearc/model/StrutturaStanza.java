package it.virtualbit.acearc.model;

public enum StrutturaStanza {
	SOPRA_SALONE, PRIMA_CASETTA, SECONDA_CASETTA, TERZA_CASETTA, CASA_RESIDENTI, ALTRO
}