package it.virtualbit.acearc.model;

public enum RuoloIscrizione {
	CAMPISTA, INTERPRETE, STAFF, RELATORE, ALTRO
}
