package it.virtualbit.acearc.model;

import java.io.*;
import java.text.*;
import java.util.*;

import javax.persistence.*;

import org.openxava.annotations.*;

import lombok.*;

@Entity @Getter @Setter
@View(members=
	"codice, struttura, numero;" +
	"posti;"
)
public class Stanza extends AceArcId implements Serializable {

	private static final long serialVersionUID = 1L;
        private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	@Column(length=10)
    private String codice;

    @Column()
	@Enumerated(EnumType.STRING)
	private StrutturaStanza struttura;

    @Column()
	private Integer numero;

    @Column()
	private Integer posti;

	public String getDescriptionForLists() {
	  return codice;
	}
}
