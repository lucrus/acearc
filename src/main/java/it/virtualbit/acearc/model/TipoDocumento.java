package it.virtualbit.acearc.model;

import java.io.*;

import javax.persistence.*;

import lombok.*;

@Entity @Getter @Setter
public class TipoDocumento extends AceArcId implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(length=50)
	private String name;
	
	@Column(length=2)
	private String abbreviazione;
	

}
