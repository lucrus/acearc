package it.virtualbit.acearc.model;

import java.io.*;
import java.util.*;

import javax.persistence.*;

import org.openxava.annotations.*;

import lombok.*;

@Entity @Getter @Setter
public class DocumentoId extends AceArcId implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ManyToOne(fetch=FetchType.LAZY,optional=true) 
    //@DescriptionsList(descriptionProperties = "abbreviazione") 
	private TipoDocumento tipo; 
	
	@Column(length=80)
    private String rilasciatoDa;

	@Column()
    private Date rilascio;

	@Column()
    private Date scadenza;

	@Column(length=80)
    private String numero;

	@ManyToOne(fetch=FetchType.LAZY, optional=false)
	@DescriptionsList(descriptionProperties = "cognome, nome, dataNascita, codiceFiscale") 
	private Contatto contatto; 

}
