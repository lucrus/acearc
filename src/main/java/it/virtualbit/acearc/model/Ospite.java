package it.virtualbit.acearc.model;

import java.io.*;
import java.util.*;

import javax.persistence.*;

import org.openxava.annotations.*;

import lombok.*;

@Entity @Getter @Setter
@Tab(properties="contatto.nome, contatto.cognome, stato, pensione, arrivo, oraDiArrivo, partenza, oraDiPartenza, adulti, bambini, neonati, consensoTrattamentoDati, note")
@View(members=
	"contatto, stato, pensione;" +
	"date [ arrivo, oraDiArrivo; partenza, oraDiPartenza ]" +
	"gruppo [ adulti; bambini; neonati ];" +
	"pagamenti [ preventivo; quotaVersata; ]," +
	"diete [ flexitariane; noMaiale; vegetariane; vegane ];" +
	"dieteSpecifiche;" +
	"consensoTrattamentoDati;" +
	"consensoImmagineWeb;" +
	"consensoNewsletter;" +
	"consensoIndirizzario;" +
	"note;"
)
public class Ospite extends AceArcId implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch=FetchType.LAZY, optional=false)
	@ReferenceView("Simple")
	private Contatto contatto;

	@Column()
	private Stato stato;
	public enum Stato { Ricevuta, Accettata, Proposta, Cancellata };

	@Column()
	private Pensione pensione;
	public enum Pensione { Completa, Mezza, Bnb, Pasto, Altro };

	@Column()
    private Date arrivo;

	@Stereotype("TIME")
	@Column()
	private String oraDiArrivo;

	@Column()
    private Date partenza;

	@Stereotype("TIME")
	@Column()
	private String oraDiPartenza;

	@Stereotype("MONEY")
	@Column()
    private Float preventivo;

	@Stereotype("MONEY")
	@Column()
    private Float quotaVersata;

	@Column()
	private Integer adulti;

	@Column()
	private Integer bambini;

	@Column()
	private Integer neonati;

	@Column()
	private Integer flexitariane;

	@Column()
	private Integer vegetariane;

	@Column()
	private Integer vegane;

	@Column()
	private Integer noMaiale;

	@Column(length=200)
	@Stereotype("MEMO")
	private String dieteSpecifiche;

	@Column()
    private Boolean consensoTrattamentoDati;

	@Column()
    private Boolean consensoImmagineWeb;

	@Column()
    private Boolean consensoIndirizzario;

	@Column()
    private Boolean consensoNewsletter;

	@Column(length=1000)
	@Stereotype("MEMO")
    private String note;
}
