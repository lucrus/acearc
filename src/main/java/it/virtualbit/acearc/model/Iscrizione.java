package it.virtualbit.acearc.model;

import java.io.*;
import java.util.*;

import javax.persistence.*;

import org.openxava.annotations.*;

import lombok.*;

@Entity @Getter @Setter
@Tab(properties="contatto.nome, contatto.cognome, campo.codice, campo.inizio, ruolo, stato, arrivo, oraDiArrivo, partenza, oraDiPartenza, quotaScelta, quotaVersata, consensoTrattamentoDati, note")
@View(members=
	"contatto, campo, stato, ruolo, dataIscrizione;" +
	"date [ arrivo, oraDiArrivo; partenza, oraDiPartenza ]" +
	"pagamenti [quotaScelta; quotaVersata];" +
	"Coda [inCoda; inCodaNuovi; inCodaComunita; inCodaPartnership]" +
	"consensoTrattamentoDati;" +
	"consensoImmagineWeb;" +
	"consensoImmagineInt;" +
	"consensoNewsletter;" +
	"consensoIndirizzario;" +
	"note;"
)
public class Iscrizione extends AceArcId implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch=FetchType.LAZY, optional=false)
	//@DescriptionsList(descriptionProperties = "cognome, nome, dataNascita, codiceFiscale")
	@ReferenceView("Simple")
	private Contatto contatto;

	@ManyToOne(fetch=FetchType.LAZY,optional=false)
	// @DescriptionsList(descriptionProperties = "codice,inizio", order = "codice,inizio")
	@DescriptionsList(descriptionProperties = "descriptionForLists", order = "codice, ${inizio} desc")
	private Campo campo;

	@Column()
	@Enumerated(EnumType.STRING)
	private RuoloIscrizione ruolo;

	@Column()
	private Stato stato;
	public enum Stato { Ricevuta, Accettata, InCoda, Cancellata };

	@Column()
	private Date dataIscrizione;

	@Column()
	private Date accettataIl;

	@Column()
	private Date arrivo;

	@Stereotype("TIME")
	@Column()
	private String oraDiArrivo;

	@Column()
	private Date partenza;

	@Stereotype("TIME")
	@Column()
	private String oraDiPartenza;

	@Stereotype("MONEY")
	@Column()
    private Float quotaScelta;

	@Stereotype("MONEY")
	@Column()
    private Float quotaVersata;

	@Column()
    private Integer inCoda;

	@Column()
    private Integer inCodaNuovi;

	@Column()
    private Integer inCodaComunita;

	@Column()
    private Integer inCodaPartnership;

	@Column()
    private Boolean consensoImmagineInt;

	@Column()
    private Boolean consensoImmagineWeb;

	@Column()
    private Boolean arrivoInPullman;

	@Column()
    private Boolean partenzaInPullman;

	@Column()
    private Boolean consensoTrattamentoDati;

	@Column()
    private Boolean consensoIndirizzario;

	@Column()
    private Boolean consensoNewsletter;

	@Column(length=1000)
	@Stereotype("MEMO")
	private String note;

	@ManyToOne(fetch=FetchType.LAZY, optional=true)
	@ReferenceView("Simple")
	private Stanza stanza;

}
