package it.virtualbit.acearc.model;

import java.io.*;

import javax.persistence.*;

import org.openxava.annotations.*;

import lombok.*;

@Entity @Getter @Setter
@View(name="Lingue")
public class Lingua extends AceArcId implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(length=50)
	private String nome;
	
	@Column(length=3)
	private String codice;
}
