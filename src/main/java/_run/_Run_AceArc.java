package _run;

import org.openxava.util.*;

/**
 * Execute this class to start the application.
 *
 * With Eclipse: Right mouse button > Run As > Java Application
 */

public class _Run_AceArc {

	public static void main(String[] args) throws Exception {
                boolean th = true;
                if (th) {
                    throw new Exception("trying to start from old main method");
                }
		DBServer.start("AceArcDB"); // To use your own database comment this line and configure web/META-INF/context.xml
		AppServer.run("AceArc"); // Use AppServer.run("") to run in root context
	}

}
